import React, { useState, useEffect } from 'react';
import '../styles/globals.css';
import 'boxicons/css/boxicons.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-datetime/css/react-datetime.css';
import 'react-datepicker/dist/react-datepicker.css';

// import 'mdbreact/dist/css/mdb.css';
import { UserProvider } from '../UserContext';

function MyApp({ Component, pageProps }) {
  //State hook for user state, define here for global scope
  const [user, setUser] = useState({
    // Initialized as an object with properties set as null
    // Proper values will be obtained from localStorage AFTER component gets rendered due to Next.JS pre-rendering
    id: null,
  });

  // Effect hook to set global user state when changes to the id property of user state is detected
  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data._id) {
          //JWT validated
          setUser({
            id: data._id,
          });
        } else {
          //JWT is invalid or non-existent
          setUser({
            id: null,
          });
        }
      });
  }, [user.id]);

  // Function for clearing local storage upon logout
  const unsetUser = () => {
    localStorage.clear();

    // Set the user global scope in the context provider to have its id set to null
    setUser({
      id: null,
    });
  };

  return (
    <UserProvider
      value={{
        user,
        setUser,
        unsetUser,
      }}>
      <Component {...pageProps} />
    </UserProvider>
  );
}

export default MyApp;
