import React from 'react';
import { useState, useContext, useEffect } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import styles from '../styles/Home.module.css';
import LoginForm from '../components/LoginForm';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Home() {
  const { user, setUser } = useContext(UserContext);

  const Router = useRouter();

  useEffect(() => {
    Swal.fire({
      title: 'Please wait...',
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
  }, []);

  // if already logged in redirect to dashboard
  useEffect(() => {
    if (user.id !== null) {
      Router.push('/dashboard');
    } else {
      setTimeout(() => Swal.close(), 2000);
    }
  }, [user]);

  function retrieveUserDetails(accessToken) {
    // Send a fetch request to decode JWT and obtain user ID and role for storing in context
    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // Set the global user state to have properties containing authenticated user's ID
        setUser({
          id: data._id,
        });
        Swal.close();
        Router.push('/dashboard');
      });
  }

  return (
    <React.Fragment>
      <Head>
        <title>Blue Edge Medical Group</title>
        <link rel='shortcut icon' href='/logo-removebg-preview.png' />
      </Head>
      <div className='login'>
        <div className='login__content'>
          <div className='login__img'>
            <img
              className='login__img-img'
              src='/Blue_edge_logo-removebg-preview.png'
            />
          </div>

          <div className='login__forms'>
            <LoginForm retrieveUserDetails={retrieveUserDetails} />
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}
