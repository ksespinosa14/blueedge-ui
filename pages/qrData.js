import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { Table } from 'react-bootstrap';

import Swal from 'sweetalert2';
import moment from 'moment';

export default function QrData() {
  const Router = useRouter();

  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [gender, setGender] = useState('');
  const [collectionDateTime, setCollectionDateTime] = useState('');
  const [testResult, setTestResult] = useState('');
  const [testType, setTestType] = useState('');
  const [igG, setIgG] = useState('');
  const [igM, setIgM] = useState('');

  useEffect(() => {
    if (Object.keys(Router.query).length === 0) return;

    Swal.fire({
      title: 'Please wait...',
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });

    async function getQrData() {
      const { token } = Router.query;
      try {
        const response = await fetch(
          `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/qrVerification`,
          {
            method: 'GET',
            headers: {
              token,
            },
          }
        );

        const data = await response.json();

        setName(
          `${data[0].lastName}, ${
            data[0].firstName
          } ${data[0].middleName.substring(0, 1)}.`
        );
        setAge(data[0].age);
        setGender(data[0].gender);
        setTestType(data[0].testType);
        setCollectionDateTime(data[0].collectionDateTime);
        setTestResult(data[0].testResult);
        setIgG(data[0].igG);
        setIgM(data[0].igM);

        Swal.close();
      } catch (err) {
        Swal.fire({
          icon: 'error',
          title: 'Something went wrong!',
          text: 'Please re-scan QR code, if error persists please contact the branch you tested on',
          allowOutsideClick: false,
          showConfirmButton: false,
        });
      }
    }
    getQrData();
  }, [Router]);

  return (
    <>
      <Head>
        <title>Blue Edge</title>
        <link rel='shortcut icon' href='/logo-removebg-preview.png' />
      </Head>
      <div
        className='d-flex flex-column align-items-center bg-light'
        style={{ minHeight: '100vh' }}>
        <img
          style={{ height: '250px', width: '250px' }}
          src='/Blue_edge_logo-removebg-preview.png'
        />

        <div
          className='bg-white p-2'
          style={{ height: '350px', width: '350px', marginTop: '-40px' }}>
          <small>Certificate Verification</small>

          <Table className='mt-2 mb-4'>
            <tbody size='sm'>
              <tr>
                <td style={{ width: '30%' }}>Name:</td>
                <td>{name}</td>
              </tr>
              <tr>
                <td>Age:</td>
                <td>{age}</td>
              </tr>
              <tr>
                <td>Gender:</td>
                <td>{gender}</td>
              </tr>
            </tbody>
          </Table>

          <h4 className='text-center mb-4'>
            {testType !== 'Antibody Blood Prick' ? (
              <span
                className={
                  testResult === 'POSITIVE' ? 'text-danger' : 'text-primary'
                }>
                {testResult}
              </span>
            ) : (
              <>
                <strong className='text-secondary'>igG: </strong>
                <strong
                  className={
                    igG === 'REACTIVE' ? 'text-success' : 'text-secondary'
                  }>
                  {igG}
                </strong>
                <br />
                <strong className='text-secondary'>igM: </strong>
                <strong
                  className={
                    igM === 'REACTIVE' ? 'text-danger' : 'text-secondary'
                  }>
                  {igM}
                </strong>
              </>
            )}
          </h4>

          <small>
            We verify that <strong>{name}</strong> tested for{' '}
            <strong>{testType}</strong> test on{' '}
            <strong>
              {moment(collectionDateTime).format('MM/DD/YYYY h:mm a')}
            </strong>{' '}
            {testType !== 'Antibody Blood Prick' ? (
              <span>
                and marked as{' '}
                <strong
                  className={
                    testResult === 'POSITIVE' ? 'text-danger' : 'text-primary'
                  }>
                  {testResult}
                </strong>
              </span>
            ) : (
              ''
            )}
          </small>
        </div>
      </div>
    </>
  );
}
