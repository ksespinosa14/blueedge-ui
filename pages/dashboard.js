import React, { useState, useEffect, useContext } from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from 'next/router';
import UserContext from '../UserContext';

import DataTable from '../components/DataTable';
import EntryForm from '../components/EntryForm';

import { Container, Button, Row, Col } from 'react-bootstrap';
import DatePicker from 'react-datepicker';

export default function Dashboard() {
  const { user, unsetUser } = useContext(UserContext);

  const [toggle, setToggle] = useState('');
  const [total, setTotal] = useState(0);

  const [startDate, setStartDate] = useState(new Date());

  const [showForm, setShowForm] = useState(false);

  const [logged, setLogged] = useState(false);

  const Router = useRouter();

  // if not logged in redirect to homepage
  useEffect(() => {
    if (localStorage.getItem('token') === null) Router.push('/');
    else setLogged(true);
    if (Router.query.recordId) {
      setShowForm(true);
    }
  }, [user]);

  //logout hook
  useEffect(() => {
    if (Router.query.view === 'Logout') {
      unsetUser();
      Router.push('/');
    }
  }, [Router]);

  function showNavbar() {
    if (toggle === '') setToggle('toggled');
    else setToggle('');
  }

  return (
    <React.Fragment>
      <Head>
        <title>Blue Edge</title>
        <link rel='shortcut icon' href='/logo-removebg-preview.png' />
      </Head>
      {!logged ? (
        ''
      ) : (
        <main id='body-pd' className={toggle === '' ? '' : 'body-pd'}>
          <header
            className={toggle === '' ? 'header' : 'header body-pd'}
            id='header'>
            <div className='header__toggle'>
              <i
                className={toggle === '' ? 'bx bx-menu' : 'bx bx-x'}
                id='header-toggle'
                onClick={() => showNavbar()}></i>
              <div className='ml-2 d-none d-sm-inline'>
                {Router.query.view === undefined
                  ? 'Dashboard'
                  : Router.query.view}
              </div>
            </div>
          </header>
          <div
            className={toggle === '' ? 'l-navbar' : 'l-navbar show'}
            id='nav-bar'>
            <nav className='nav'>
              <div>
                <a href='#' className='nav__logo'>
                  <i className='bx bxs-pie-chart-alt-2 nav__logo-icon'></i>
                  <span className='nav__logo-name'>Blue Edge</span>
                </a>
                <div className='nav__list'>
                  <Link href='/dashboard'>
                    <a
                      className={
                        Router.query.view === undefined
                          ? 'nav__link active'
                          : 'nav__link'
                      }>
                      <i className='bx bx-grid-alt nav__icon'></i>
                      <span className='nav__name'>Dashboard</span>
                    </a>
                  </Link>
                </div>
              </div>

              <Link href='/dashboard?view=Logout'>
                <a
                  href='#'
                  className={
                    Router.query.view === 'Logout'
                      ? 'nav__link active'
                      : 'nav__link'
                  }>
                  <i className='bx bx-log-out nav__icon'></i>
                  <span className='nav__name'>Logout</span>
                </a>
              </Link>
            </nav>
          </div>

          <Row className='my-3'>
            <Col sm='8' md='8' lg='10'>
              <Button
                variant='primary'
                onClick={() => {
                  Router.push('');
                  setShowForm(!showForm);
                }}>
                {showForm ? 'Back' : 'New Entry'}
              </Button>
            </Col>
            <Col sm='4' md='4' lg='2'>
              {showForm ? (
                ''
              ) : (
                <DatePicker
                  selected={startDate}
                  onChange={(date) => setStartDate(date)}
                />
              )}
            </Col>
          </Row>

          {showForm ? (
            <EntryForm showForm={showForm} setShowForm={setShowForm} />
          ) : (
            <DataTable
              startDate={startDate}
              setStartDate={setStartDate}
              showForm={showForm}
              setShowForm={setShowForm}
            />
          )}
        </main>
      )}
    </React.Fragment>
  );
}
