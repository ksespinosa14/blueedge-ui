import React, { useState, useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import UserContext from '../UserContext';
import { Table, ButtonGroup, Button, Container, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import moment from 'moment';

export default function DataTable({
  startDate,
  setStartDate,
  showForm,
  setShowForm,
}) {
  const { user, unsetUser } = useContext(UserContext);
  const [records, setRecords] = useState([]);

  const Router = useRouter();

  useEffect(() => {
    Swal.fire({
      title: 'Please wait...',
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });

    async function fetchRecord() {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/record/getDay`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
          body: JSON.stringify({
            startDate,
          }),
        }
      );

      const data = await response.json();
      setRecords(data);

      Swal.close();
    }
    fetchRecord();
  }, [startDate]);

  async function generateQR(recordId) {
    Swal.fire({
      title: 'Please wait...',
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });

    const response = await fetch(
      `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/record/${recordId}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      }
    );

    const data = await response.json();

    if (!data[0].qrString) {
      const getQR = await fetch(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/generateQR/${recordId}`,
        {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        }
      );

      const qrData = await getQR.json();
      showQR(qrData.qrString, recordId);
    } else {
      showQR(data[0].qrString, recordId);
    }
  }

  function showQR(qrString, recordId) {
    Swal.fire({
      imageUrl: qrString,
      imageAlt: 'QR Code',
      text: recordId,
      showConfirmButton: false,
    });
  }

  const recordsList = records.map((record) => {
    return (
      <tr key={record._id}>
        <td>{`${record.lastName}, ${record.firstName} ${record.middleName}`}</td>
        <td>{record.age}</td>
        <td>{record.gender}</td>
        <td>{record.address}</td>
        <td>{record.contactNumber}</td>
        <td>{record.testType}</td>
        <td>{moment(record.collectionDateTime).format('MM/DD/YYYY h:mm a')}</td>
        <td>
          {record.testType !== 'Antibody Blood Prick' ? (
            <strong
              className={
                record.testResult === 'POSITIVE'
                  ? 'text-danger'
                  : 'text-primary'
              }>
              {record.testResult}
            </strong>
          ) : (
            <>
              <div>
                <strong className='text-primary'>igG: </strong>
                <strong
                  className={
                    record.igG === 'REACTIVE'
                      ? 'text-success'
                      : 'text-secondary'
                  }>
                  {record.igG}
                </strong>
              </div>
              <div>
                <strong className='text-primary'>igM: </strong>
                <strong
                  className={
                    record.igM === 'REACTIVE' ? 'text-danger' : 'text-secondary'
                  }>
                  {record.igM}
                </strong>
              </div>
            </>
          )}
        </td>
        <td>
          <ButtonGroup aria-label='Actions'>
            <Button
              variant='success'
              onClick={() => {
                setShowForm(true);
                Router.push({
                  query: { recordId: record._id },
                });
              }}>
              Edit
            </Button>
            <Button variant='secondary' onClick={() => generateQR(record._id)}>
              QR
            </Button>
            <Button variant='danger' onClick={() => handleDelete(record._id)}>
              Delete
            </Button>
          </ButtonGroup>
        </td>
      </tr>
    );
  });

  function handleDelete(recordId) {
    Swal.fire({
      icon: 'question',
      title: 'Are your sure you want to delete this entry?',
      showDenyButton: true,
      showCancelButton: true,
      showConfirmButton: false,
      denyButtonText: `Delete`,
    }).then(async (result) => {
      if (result.isDenied) {
        Swal.fire({
          title: 'Please wait...',
          allowOutsideClick: false,
          didOpen: () => {
            Swal.showLoading();
          },
        });

        const response = await fetch(
          `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/record/${recordId}`,
          {
            method: 'DELETE',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
          }
        );

        const data = await response.json();
        setStartDate(new Date());
        if (data) Swal.fire('Successfully deleted!', '', 'success');
        else
          Swal.fire({
            icon: 'error',
            title: 'Something Went Wrong!',
            text: 'Refresh and Retry, if the error persists contact your administrator',
          });
      }
    });
  }

  return (
    <React.Fragment>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Age</th>
            <th>Gender</th>
            <th>Address</th>
            <th>Contact #</th>
            <th>Type</th>
            <th>Collection Time</th>
            <th>Result</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {records.length === 0 ? (
            <tr>
              <td className='text-center' colSpan='9'>
                No Records Found
              </td>
            </tr>
          ) : (
            recordsList
          )}
        </tbody>
      </Table>
    </React.Fragment>
  );
}
