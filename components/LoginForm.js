import React, { useState, useContext } from 'react';
import { useRouter } from 'next/router';
import { Alert } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function LoginForm({ retrieveUserDetails }) {
  const { user, setUser } = useContext(UserContext);
  const Router = useRouter();

  const [userName, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [alertMsg, setAlertMsg] = useState('');

  function authenticate() {
    if (userName === '' || password === '')
      setAlertMsg('Please fill-out all fields.');
    else {
      setAlertMsg('');
      Swal.fire({
        title: 'Please wait...',
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });
      fetch(`${process.env.NEXT_PUBLIC_API_BASE_URL}/users/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          userName: userName,
          password: password,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          // Successful authentication will return a JWT via the response accessToken property
          if (data.accessToken) {
            // Store JWT in local storage
            localStorage.setItem('token', data.accessToken);
            retrieveUserDetails(data.accessToken);
          } else {
            // Authentication failure
            Swal.close();
            if (data.error === 'does-not-exist') {
              setAlertMsg('Invalid Username');
            } else if (data.error === 'inactive-account') {
              setAlertMsg(
                'Please contact administrator to activate your account.'
              );
            } else if (data.error === 'incorrect-password') {
              setAlertMsg('Invalid Password');
            }
          }
        });
    }
  }

  async function checkIfUserExists(login) {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/username-exists`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          userName: login,
        }),
      }
    );

    const data = await response.json();

    return data;
  }

  async function register(login, password) {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          userName: login,
          password: password,
        }),
      }
    );

    const data = await response.json();

    return data;
  }

  function swalError() {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Something went wrong!',
    });
  }

  function createUser() {
    Swal.fire({
      title: 'Login Form',
      html: `
        <input type="text" id="login" class="swal2-input" placeholder="Username">
        <input type="password" id="password1" class="swal2-input" placeholder="Password">
        <input type="password" id="password2" class="swal2-input" placeholder="ConfirmPassword">
        `,
      confirmButtonText: 'Create User',
      confirmButtonColor: '#2d58c5',
      focusConfirm: false,
      preConfirm: () => {
        const login = Swal.getPopup().querySelector('#login').value;
        const password1 = Swal.getPopup().querySelector('#password1').value;
        const password2 = Swal.getPopup().querySelector('#password2').value;

        if (!login || !password1 || !password2) {
          Swal.showValidationMessage(`Please enter all details`);
        }
        if (password1 !== password2) {
          Swal.showValidationMessage(`Password doesn't match`);
        }

        return { login: login, password: password1 };
      },
    }).then(async (result) => {
      Swal.fire({
        title: 'Please wait...',
        allowOutsideClick: false,
        didOpen: () => {
          Swal.showLoading();
        },
      });

      if (!result.value) {
        Swal.close();
        return;
      }

      try {
        const userNameExist = await checkIfUserExists(result.value.login);
        if (userNameExist) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Username already exists',
          });
        } else {
          try {
            const tryRegister = await register(
              result.value.login,
              result.value.password
            );
            if (tryRegister) {
              Swal.fire(
                'Success!',
                'Inform the administrator to activate your account',
                'success'
              );
            } else {
              swalError();
            }
          } catch (err) {
            swalError();
            console.log(err);
          }
        }
      } catch (err) {
        swalError();
        console.log(err);
      }
    });
  }

  return (
    <React.Fragment>
      <form className='login__register' id='login-in'>
        <h1 className='login__title'>Sign In</h1>
        {alertMsg ? (
          <Alert className='mx-auto' variant='danger'>
            {alertMsg}
          </Alert>
        ) : (
          ''
        )}
        <div className='login__box'>
          <i className='bx bx-at login__icon'></i>
          <input
            type='text'
            placeholder='Username'
            className='login__input'
            value={userName}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>

        <div className='login__box'>
          <i className='bx bx-lock login__icon'></i>
          <input
            type='password'
            placeholder='Password'
            className='login__input'
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <a className='login__button' onClick={() => authenticate()}>
          Sign In
        </a>

        <div>
          <span className='login__account'>Don&apos;t have an Account? </span>
          <span
            className='login__signin'
            id='sign-up'
            onClick={() => createUser()}>
            Create User
          </span>
        </div>
      </form>
    </React.Fragment>
  );
}
