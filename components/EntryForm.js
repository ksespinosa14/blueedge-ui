import React, { useState, useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import UserContext from '../UserContext';

import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import moment from 'moment';
import Datetime from 'react-datetime';
import Swal from 'sweetalert2';

export default function EntryForm({ showForm, setShowForm }) {
  const { user, unsetUser } = useContext(UserContext);

  const Router = useRouter();

  const [firstName, setFirstName] = useState('');
  const [middleName, setMiddleName] = useState('');
  const [lastName, setLastName] = useState('');
  const [age, setAge] = useState('');
  const [gender, setGender] = useState(null);
  const [address, setAddress] = useState('');
  const [contactNumber, setContactNumber] = useState('');
  const [testType, setTestType] = useState(null);
  const [collectionDateTime, setCollectionDateTime] = useState(new Date());
  const [testResult, setTestResult] = useState(null);
  const [igG, setIgG] = useState(null);
  const [igM, setIgM] = useState(null);

  useEffect(() => {
    if (Object.keys(Router.query).length === 0) return;
    Swal.fire({
      title: 'Please wait...',
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    async function fetchRecord() {
      const { recordId } = Router.query;
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/record/${recordId}`,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        }
      );

      const data = await response.json();

      setFirstName(data[0].firstName);
      setMiddleName(data[0].middleName);
      setLastName(data[0].lastName);
      setAge(data[0].age);
      setGender(data[0].gender);
      setAddress(data[0].address);
      setContactNumber(data[0].contactNumber);
      setTestType(data[0].testType);
      setCollectionDateTime(new Date(data[0].collectionDateTime));
      setTestResult(data[0].testResult);
      setIgG(data[0].igG);
      setIgM(data[0].igM);

      Swal.close();
    }
    fetchRecord();
  }, [Router.query]);

  function handleTestType(value) {
    setTestType(value);
    setTestResult(null);
    setIgG(null);
    setIgM(null);
  }

  function validateForm(e) {
    e.preventDefault();
    if (
      (testType !== 'Antibody Blood Prick' && !testResult) ||
      (testType === 'Antibody Blood Prick' && (!igG || !igM))
    ) {
      Swal.fire({
        icon: 'error',
        title: 'Form Error',
        text: 'Please fill out all fields!',
      });
      return;
    } else if (
      !firstName ||
      !middleName ||
      !lastName ||
      !age ||
      !address ||
      !contactNumber ||
      !testType
    ) {
      Swal.fire({
        icon: 'error',
        title: 'Form Error',
        text: 'Please fill out all fields!',
      });
      return;
    }

    saveRecord();
  }

  async function saveRecord() {
    Swal.fire({
      title: 'Please wait...',
      allowOutsideClick: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_API_BASE_URL}/users/record`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify({
          recordId: Router.query.recordId,
          firstName: firstName.toUpperCase(),
          middleName: middleName.toUpperCase(),
          lastName: lastName.toUpperCase(),
          age,
          gender,
          address: address.toUpperCase(),
          contactNumber,
          testType,
          collectionDateTime,
          testResult,
          igG,
          igM,
        }),
      }
    );

    const data = await response.json();

    if (data) {
      setFirstName('');
      setMiddleName('');
      setLastName('');
      setAge('');
      setGender(null);
      setAddress('');
      setContactNumber('');
      setTestType(null);
      setCollectionDateTime('');
      setTestResult(null);
      setIgG(null);
      setIgM(null);

      Swal.fire({
        icon: 'success',
        title: 'Success!',
      }).then(function () {
        Router.push({
          query: {},
        });
        setShowForm(!showForm);
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Something Went Wrong!',
        text: 'Refresh and Retry, if the error persists contact your administrator',
      });
    }
  }

  return (
    <>
      <Container>
        <Form onSubmit={validateForm}>
          <Form.Group className='mb-3' controlId='patentInformation'>
            <Form.Label>
              <strong>Patient Information</strong>
            </Form.Label>
            <Row className='my-3'>
              <Col xs={12} sm={4}>
                <Form.Control
                  type='text'
                  placeholder='First Name'
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                />
              </Col>
              <Col xs={12} sm={4}>
                <Form.Control
                  type='text'
                  placeholder='Middle Name'
                  value={middleName}
                  onChange={(e) => setMiddleName(e.target.value)}
                />
              </Col>
              <Col xs={12} sm={4}>
                <Form.Control
                  type='text'
                  placeholder='Last Name'
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
              </Col>
            </Row>

            <Row className='my-3'>
              <Col xs={12} sm={4}>
                <Form.Control
                  type='Number'
                  placeholder='Age'
                  value={age}
                  onChange={(e) => setAge(e.target.value)}
                />
              </Col>
              <Col xs={12} sm={4}>
                <Form.Control
                  type='text'
                  placeholder='Address'
                  value={address}
                  onChange={(e) => setAddress(e.target.value)}
                />
              </Col>
              <Col xs={12} sm={4}>
                <Form.Control
                  type='Number'
                  placeholder='Contact Number'
                  value={contactNumber}
                  onChange={(e) => setContactNumber(e.target.value)}
                />
              </Col>
            </Row>
          </Form.Group>

          <Form.Group className='mb-3' controlId='gender'>
            <div>
              <Form.Check
                type='radio'
                label='MALE'
                name='gender'
                value='MALE'
                checked={gender === 'MALE'}
                onChange={(e) => setGender(e.target.value)}
              />
              <Form.Check
                type='radio'
                label='FEMALE'
                name='gender'
                value='FEMALE'
                checked={gender === 'FEMALE'}
                onChange={(e) => setGender(e.target.value)}
              />
            </div>
          </Form.Group>

          <Form.Group className='mb-3' controlId='testType'>
            <Form.Label>
              <strong>Type of Test</strong>
            </Form.Label>
            <div>
              <Form.Check
                type='radio'
                label='Antigen Swab'
                name='testType'
                value='Antigen Swab'
                checked={testType === 'Antigen Swab'}
                onChange={(e) => handleTestType(e.target.value)}
              />
              <Form.Check
                type='radio'
                label='Antigen Saliva'
                name='testType'
                value='Antigen Saliva'
                checked={testType === 'Antigen Saliva'}
                onChange={(e) => handleTestType(e.target.value)}
              />
              <Form.Check
                type='radio'
                label='Antibody Blood Prick'
                name='testType'
                value='Antibody Blood Prick'
                checked={testType === 'Antibody Blood Prick'}
                onChange={(e) => handleTestType(e.target.value)}
              />
            </div>
          </Form.Group>

          <Form.Group className='mb-3' controlId='collectionTime'>
            <Form.Label>
              <strong>Collection Time</strong>
              <span className='text-danger'> *DON&apos;T FORGET TO UPDATE</span>
            </Form.Label>
            <Row>
              <Col xs={12} sm={4}>
                <Datetime
                  inputProps={{ placeholder: 'Collection Date and Time' }}
                  value={collectionDateTime}
                  onChange={(val) => {
                    setCollectionDateTime(val);
                  }}
                />
              </Col>
              <Col></Col>
              <Col></Col>
            </Row>
          </Form.Group>

          {testType === null ? (
            ''
          ) : testType !== 'Antibody Blood Prick' ? (
            <Form.Group className='mb-3' controlId='testResult'>
              <Form.Label>
                <strong>Test Result</strong>
              </Form.Label>
              <div>
                <Form.Check
                  inline
                  type='radio'
                  label='POSITIVE'
                  name='testResult'
                  value='POSITIVE'
                  checked={testResult === 'POSITIVE'}
                  onChange={(e) => setTestResult(e.target.value)}
                />
                <Form.Check
                  inline
                  type='radio'
                  label='NEGATIVE'
                  name='testResult'
                  value='NEGATIVE'
                  checked={testResult === 'NEGATIVE'}
                  onChange={(e) => setTestResult(e.target.value)}
                />
              </div>
            </Form.Group>
          ) : (
            <>
              <Form.Group className='mb-3' controlId='IgG'>
                <Form.Label>
                  <strong>IgG</strong>
                </Form.Label>
                <div>
                  <Form.Check
                    inline
                    type='radio'
                    label='REACTIVE'
                    name='IgG'
                    value='REACTIVE'
                    checked={igG === 'REACTIVE'}
                    onChange={(e) => setIgG(e.target.value)}
                  />
                  <Form.Check
                    inline
                    type='radio'
                    label='NON REACTIVE'
                    name='IgG'
                    value='NON REACTIVE'
                    checked={igG === 'NON REACTIVE'}
                    onChange={(e) => setIgG(e.target.value)}
                  />
                </div>
              </Form.Group>

              <Form.Group className='mb-3' controlId='IgM'>
                <Form.Label>
                  <strong>IgM</strong>
                </Form.Label>
                <div>
                  <Form.Check
                    inline
                    type='radio'
                    label='REACTIVE'
                    name='IgM'
                    value='REACTIVE'
                    checked={igM === 'REACTIVE'}
                    onChange={(e) => setIgM(e.target.value)}
                  />
                  <Form.Check
                    inline
                    type='radio'
                    label='NON REACTIVE'
                    name='IgM'
                    value='NON REACTIVE'
                    checked={igM === 'NON REACTIVE'}
                    onChange={(e) => setIgM(e.target.value)}
                  />
                </div>
              </Form.Group>
            </>
          )}

          <Button variant='primary' type='submit'>
            Submit
          </Button>
        </Form>
      </Container>
    </>
  );
}
